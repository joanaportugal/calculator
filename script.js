let calculate = document.getElementById("calculate");

const btns = document.getElementsByClassName("btn");
const equals = document.getElementById("equals");

function canBeCalculated() {
  try {
    return eval(calculate.value);
  } catch (error) {
    return false;
  }
}

equals.addEventListener("click", () => {
  if (canBeCalculated()) {
    calculate.value = eval(calculate.value);
  } else {
    calculate.value = 0;
  }
});

for (const btn of btns) {
  btn.addEventListener("click", function () {
    switch (this.id) {
      case "n0":
        calculate.value += "0";
        return;
      case "n1":
        calculate.value += "1";
        return;
      case "n2":
        calculate.value += "2";
        return;
      case "n3":
        calculate.value += "3";
        return;
      case "n4":
        calculate.value += "4";
        return;
      case "n5":
        calculate.value += "5";
        return;
      case "n6":
        calculate.value += "6";
        return;
      case "n7":
        calculate.value += "7";
        return;
      case "n8":
        calculate.value += "8";
        return;
      case "n9":
        calculate.value += "9";
        return;
      case "clear":
        calculate.value = "";
        return;
      case "parOpen":
        calculate.value += "(";
        return;
      case "parClose":
        calculate.value += ")";
        return;
      case "decimal":
        calculate.value += ".";
        return;
      case "divide":
        calculate.value += "/";
        return;
      case "multiply":
        calculate.value += "*";
        return;
      case "minus":
        calculate.value += "-";
        return;
      case "plus":
        calculate.value += "+";
        return;
    }
  });
}
